# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'Linux on Marvell MMP'
copyright = 'linux-mmp contributors'
author = 'linux-mmp contributors'

highlight_language = 'none'

extensions = ['sphinx.ext.autosectionlabel']
autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 1
