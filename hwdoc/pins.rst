.. SPDX-License-Identifier: GPL-2.0-or-later

MMP3/MMP2 Pins
==============

==============  ======  ===========  ===========  ===========  ===========  ===========  ===========  ===========  ===========
Register        Pin     Function 0   Function 1   Function 2   Function 3   Function 4   Function 5   Function 6   Function 7
==============  ======  ===========  ===========  ===========  ===========  ===========  ===========  ===========  ===========
0xd401e000      D19     USIM         GPIO_102     FSIC         KP_DK        LCD
0xd401e004      C19     USIM         GPIO_103     FSIC         KP_DK        LCD
0xd401e008      B19     USIM         GPIO_142     FSIC         KP_DK
0xd401e00c      A20     GPIO_124     MMC1         LCD          MMC3         NAND
0xd401e010      E20     GPIO_125     MMC1         LCD          MMC3         NAND
0xd401e014      AF11    GPIO_126     MMC1         LCD          MMC3         NAND
0xd401e018      AE10    GPIO_127                  LCD          MMC3         NAND
0xd401e01c      AH11    GPIO_128                  LCD          MMC3         NAND
0xd401e020      AF10    GPIO_129     MMC1         LCD          MMC3         NAND
0xd401e024      AD10    GPIO_130     MMC1         LCD          MMC3         NAND
0xd401e028      D20     GPIO_131     MMC1                      MSP
0xd401e02c      B20     GPIO_132     MMC1         PRI_JTAG     MSP          SSP3         AAS_JTAG
0xd401e030      A21     GPIO_133     MMC1         PRI_JTAG     MSP          SSP3         AAS_JTAG
0xd401e034      B21     GPIO_134     MMC1         PRI_JTAG     MSP          SSP3         AAS_JTAG
0xd401e038      F20     GPIO_135                  LCD          MMC3         NAND
0xd401e03c      C21     GPIO_136     MMC1         PRI_JTAG     MSP          SSP3         AAS_JTAG
0xd401e040      D21     GPIO_137     HDMI         LCD          MSP
0xd401e044      B22     GPIO_138                  LCD          MMC3         SMC
0xd401e048      AC10    GPIO_139     MMC1         PRI_JTAG     MSP                       AAS_JTAG
0xd401e04c      A22     GPIO_140     MMC1         LCD                                    UART2        UART1
0xd401e050      C22     GPIO_141     MMC1         LCD                                    UART2        UART1
0xd401e054      G21     GPIO_0       KP_MK                     SPI
0xd401e058      G22     GPIO_1       KP_MK                     SPI
0xd401e05c      B23     GPIO_2       KP_MK                     SPI
0xd401e060      D22     GPIO_3       KP_MK                     SPI
0xd401e064      A23     GPIO_4       KP_MK
0xd401e068      C23     GPIO_5       KP_MK                     SPI
0xd401e06c      E23     GPIO_6       KP_MK                     SPI
0xd401e070      H22     GPIO_7       KP_MK                     SPI
0xd401e074      F23     GPIO_8       KP_MK
0xd401e078      A24     GPIO_9       KP_MK
0xd401e07c      D23     GPIO_10      KP_MK
0xd401e080      B24     GPIO_11      KP_MK
0xd401e084      D24     GPIO_12      KP_MK                     CCIC1
0xd401e088      G23     GPIO_13      KP_MK                     CCIC1
0xd401e08c      J22     GPIO_14      KP_MK                     CCIC1
0xd401e090      E24     GPIO_15      KP_MK        KP_DK        CCIC1
0xd401e094      G24     GPIO_16      KP_DK        ROT          CCIC1
0xd401e098      F24     GPIO_17      KP_DK        ROT          CCIC1
0xd401e09c      H23     GPIO_18      KP_DK        ROT          CCIC1
0xd401e0a0      A25     GPIO_19      KP_DK        ROT          CCIC1
0xd401e0a4      B25     GPIO_20      KP_DK        TB           CCIC1
0xd401e0a8      K22     GPIO_21      KP_DK        TB           CCIC1
0xd401e0ac      C25     GPIO_22      KP_DK        TB           CCIC1
0xd401e0b0      D25     GPIO_23      KP_DK        TB           CCIC1
0xd401e0b4      E25     GPIO_24      I2S          VCXO_OUT
0xd401e0b8      G25     GPIO_25      I2S          HDMI         SSPA2
0xd401e0bc      J23     GPIO_26      I2S          HDMI         SSPA2
0xd401e0c0      H24     GPIO_27      I2S          HDMI         SSPA2
0xd401e0c4      L22     GPIO_28      I2S                       SSPA2
0xd401e0c8      A26     GPIO_29      UART1        KP_MK                                               AAS_SPI
0xd401e0cc      B26     GPIO_30      UART1        KP_MK                                               AAS_SPI
0xd401e0d0      K23     GPIO_31      UART1        KP_MK                                               AAS_SPI
0xd401e0d4      C26     GPIO_32      UART1        KP_MK                                               AAS_SPI
0xd401e0d8      D26     GPIO_33      SSPA2        I2S
0xd401e0dc      B27     GPIO_34      SSPA2        I2S
0xd401e0e0      J24     GPIO_35      SSPA2        I2S
0xd401e0e4      M22     GPIO_36      SSPA2        I2S
0xd401e0e8      H25     GPIO_37      MMC2         SSP1         TWSI2        UART2        UART3        AAS_SPI      AAS_TWSI
0xd401e0ec      C27     GPIO_38      MMC2         SSP1         TWSI2        UART2        UART3        AAS_SPI      AAS_TWSI
0xd401e0f0      L23     GPIO_39      MMC2         SSP1         TWSI2        UART2        UART3        AAS_SPI      AAS_TWSI
0xd401e0f4      C28     GPIO_40      MMC2         SSP1         TWSI2        UART2        UART3        AAS_SPI      AAS_TWSI
0xd401e0f8      N21     GPIO_41      MMC2         TWSI5
0xd401e0fc      D27     GPIO_42      MMC2         TWSI5
0xd401e100      F26     GPIO_43      TWSI2        UART4        SSP1         UART2        UART3                     AAS_TWSI
0xd401e104      G26     GPIO_44      TWSI2        UART4        SSP1         UART2        UART3                     AAS_TWSI
0xd401e108      E27     GPIO_45      UART1        UART4        SSP1         UART2        UART3
0xd401e10c      K24     GPIO_46      UART1        UART4        SSP1         UART2        UART3
0xd401e110      H26     GPIO_47      UART2        SSP2         TWSI6        CAM2         AAS_SPI      AAS_GPIO
0xd401e114      N22     GPIO_48      UART2        SSP2         TWSI6        CAM2         AAS_SPI      AAS_GPIO
0xd401e118      M23     GPIO_49      UART2        SSP2         PWM          CCIC2        AAS_SPI
0xd401e11c      F27     GPIO_50      UART2        SSP2         PWM          CCIC2        AAS_SPI
0xd401e120      J25     GPIO_51      UART3        ROT          AAS_GPIO     PWM
0xd401e124      D28     GPIO_52      UART3        ROT          AAS_GPIO     PWM
0xd401e128      E28     GPIO_53      UART3        TWSI2        VCXO_REQ                  PWM                       AAS_TWSI
0xd401e12c      L24     GPIO_54      UART3        TWSI2        VCXO_OUT     HDMI         PWM                       AAS_TWSI
0xd401e130      F28     GPIO_55      SSP2         SSP1         UART2        ROT          TWSI2        SSP3         AAS_TWSI
0xd401e134      G27     GPIO_56      SSP2         SSP1         UART2        ROT          TWSI2        KP_DK        AAS_TWSI
0xd401e138      P22     GPIO_57      SSP2_RX      SSP1_TXRX    SSP2_FRM     SSP1_RX      VCXO_REQ     KP_DK
0xd401e13c      G28     GPIO_58      SSP2         SSP1_RX      SSP1_FRM     SSP1_TXRX    VCXO_REQ     KP_DK
0xd401e140      L26     TWSI1                                                                                               # XXX
0xd401e144      K28     TWSI1                                                                                               # XXX
0xd401e148      R23     GPIO_123     SLEEP_IND    ONE_WIRE     32K_CLKOUT
0xd401e14c      L28     PRI_JTAG     GPIO_156     PWM
0xd401e150      N25     PRI_JTAG     GPIO_157     PWM
0xd401e154      M27     PRI_JTAG     GPIO_158     PWM
0xd401e158      N26     PRI_JTAG     GPIO_159     PWM
0xd401e15c      M28     PRI_JTAG
0xd401e160      L27     G_CLKREQ     ONE_WIRE
0xd401e164      U21     G_CLKOUT     32K_CLKOUT   HDMI
0xd401e168      N27     VCXO_REQ     ONE_WIRE     PLL
0xd401e16c      T24     VCXO_OUT     32K_CLKOUT
0xd401e170      W23     GPIO_74      LCD          SMC          MMC4         SSP3         UART2        UART4        TIPU
0xd401e174      V25     GPIO_75      LCD          SMC          MMC4         SSP3         UART2        UART4        TIPU
0xd401e178      W22     GPIO_76      LCD          SMC          MMC4         SSP3         UART2        UART4        TIPU
0xd401e17c      Y25     GPIO_77      LCD          SMC          MMC4         SSP3         UART2        UART4        TIPU
0xd401e180      W24     GPIO_78      LCD          HDMI         MMC4                      SSP4         AAS_SPI      TIPU
0xd401e184      Y22     GPIO_79      LCD          AAS_GPIO     MMC4                      SSP4         AAS_SPI      TIPU
0xd401e188      Y23     GPIO_80      LCD          AAS_GPIO     MMC4                      SSP4         AAS_SPI      TIPU
0xd401e18c      Y24     GPIO_81      LCD          AAS_GPIO     MMC4                      SSP4         AAS_SPI      TIPU
0xd401e190      AA20    GPIO_82      LCD                       MMC4                                   CCIC2        TIPU
0xd401e194      AA24    GPIO_83      LCD                       MMC4                                   CCIC2        TIPU
0xd401e198      AA23    GPIO_84      LCD          SMC          MMC2                      TWSI5        AAS_TWSI     TIPU
0xd401e19c      AB21    GPIO_85      LCD          SMC          MMC2                      TWSI5        AAS_TWSI     TIPU
0xd401e1a0      AB24    GPIO_86      LCD          SMC          MMC2                      TWSI6        CCIC2        TIPU
0xd401e1a4      AA25    GPIO_87      LCD          SMC          MMC2                      TWSI6        CCIC2        TIPU
0xd401e1a8      AB22    GPIO_88      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1ac      AB25    GPIO_89      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1b0      AB23    GPIO_90      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1b4      AB27    GPIO_91      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1b8      AB28    GPIO_92      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1bc      AB20    GPIO_93      LCD          AAS_GPIO     MMC2                                   CCIC2        TIPU
0xd401e1c0      AC24    GPIO_94      LCD          AAS_GPIO     SPI                       AAS_SPI      CCIC2        TIPU
0xd401e1c4      AC21    GPIO_95      LCD          TWSI3        SPI          AAS_DEU_EX   AAS_SPI      CCIC2        TIPU
0xd401e1c8      AC23    GPIO_96      LCD          TWSI3        SPI          AAS_DEU_EX   AAS_SPI                   TIPU
0xd401e1cc      AA19    GPIO_97      LCD          TWSI6        SPI          AAS_DEU_EX   AAS_SPI                   TIPU
0xd401e1d0      EC25    GPIO_98      LCD          TWSI6        SPI          ONE_WIRE                               TIPU
0xd401e1d4      AC27    GPIO_99      LCD          SMC          SPI          TWSI5                                  TIPU
0xd401e1d8      AC26    GPIO_100     LCD          SMC          SPI          TWSI5                                  TIPU
0xd401e1dc      AB19    GPIO_101     LCD          SMC          SPI                                                 TIPU
0xd401e1e0      AF28    NAND         GPIO_168     MMC3
0xd401e1e4      AF25    NAND         GPIO_167     MMC3
0xd401e1e8      AF26    NAND         GPIO_166     MMC3
0xd401e1ec      AE23    NAND         GPIO_165     MMC3
0xd401e1f0      AE21    NAND         GPIO_107                               NAND
0xd401e1f4      AF27    NAND         GPIO_106                               NAND
0xd401e1f8      AE22    NAND         GPIO_105                               NAND
0xd401e1fc      AE25    NAND         GPIO_104                               NAND
0xd401e200      AE26    NAND         GPIO_111     MMC3
0xd401e204      AD25    NAND         GPIO_164     MMC3
0xd401e208      AD28    NAND         GPIO_163     MMC3
0xd401e20c      AD24    NAND         GPIO_162     MMC3
0xd401e210      AD26    NAND         GPIO_161                               NAND
0xd401e214      AD25    NAND         GPIO_110                               NAND
0xd401e218      AD24    NAND         GPIO_109                               NAND
0xd401e21c      AD23    NAND         GPIO_108                               NAND
0xd401e220      AF23    NAND         GPIO_143     SMC                       NAND
0xd401e224      AF24    NAND         GPIO_144     SMC_INT      SMC          NAND
0xd401e228      AC19    SMC          GPIO_145                               SMC
0xd401e22c      AC20    SMC          GPIO_146                               SMC
0xd401e230      AG25    NAND         GPIO_147                               NAND
0xd401e234      AG26    NAND         GPIO_148                               NAND
0xd401e238      AA16    NAND         GPIO_149
0xd401e23c      AH26    NAND         GPIO_150                               NAND
0xd401e240      AG24    SMC          GPIO_151     MMC3
0xd401e244      AH25    NAND         GPIO_112     MMC3         SMC
0xd401e248      AC18    SMC          GPIO_152                               SMC
0xd401e24c      AD18    SMC          GPIO_153                               SMC
0xd401e250      AH24    NAND         GPIO_160     SMC                       NAND
0xd401e254      AB17    SMC_INT      GPIO_154     SMC                       NAND
0xd401e258      AE19    EXT_DMA      GPIO_155     SMC                       EXT_DMA
0xd401e25c      AC17    SMC          GPIO_113     EXT_DMA      MMC3         SMC          HDMI
0xd401e260      N23     GPIO_115                  AC           UART4        UART3        SSP1
0xd401e264      H27     GPIO_116                  AC           UART4        UART3        SSP1
0xd401e268      H28     GPIO_117                  AC           UART4        UART3        SSP1
0xd401e26c      J28     GPIO_118                  AC           UART4        UART3        SSP1
0xd401e270      M24     GPIO_119                  CA           SSP3
0xd401e274      J27     GPIO_120                  CA           SSP3
0xd401e278      K26     GPIO_121                  CA           SSP3
0xd401e27c      L25     GPIO_122                  CA           SSP3
0xd401e280      AD13    GPIO_59      CCIC1        ULPI         MMC3         CCIC2        UART3        UART4
0xd401e284      AE13    GPIO_60      CCIC1        ULPI         MMC3         CCIC2        UART3        UART4
0xd401e288      AF13    GPIO_61      CCIC1        ULPI         MMC3         CCIC2        UART3        HDMI
0xd401e28c      AH13    GPIO_62      CCIC1        ULPI         MMC3         CCIC2        UART3
0xd401e290      AG13    GPIO_63      CCIC1        ULPI         MMC3         CCIC2        MSP          UART4
0xd401e294      AC12    GPIO_64      CCIC1        ULPI         MMC3         CCIC2        MSP          UART4
0xd401e298      AD12    GPIO_65      CCIC1        ULPI         MMC3         CCIC2        MSP          UART4
0xd401e29c      AE12    GPIO_66      CCIC1        ULPI         MMC3         CCIC2        MSP          UART4
0xd401e2a0      AF12    GPIO_67      CCIC1        ULPI         MMC3         CCIC2        MSP
0xd401e2a4      AG12    GPIO_68      CCIC1        ULPI         MMC3         CCIC2        MSP          LCD
0xd401e2a8      AH12    GPIO_69      CCIC1        ULPI         MMC3         CCIC2                     LCD
0xd401e2ac      AC11    GPIO_70      CCIC1        ULPI         MMC3         CCIC2        MSP          LCD
0xd401e2b0      AD11    GPIO_71      TWSI3                     PWM                                    LCD          AAS_TWSI
0xd401e2b4      AG11    GPIO_72      TWSI3        HDMI         PWM                                    LCD          AAS_TWSI
0xd401e2b8      AE11    GPIO_73      VCXO_REQ     32K_CLKOUT   PWM          VCXO_OUT                  LCD
0xd401e2bc      D18     TWSI4        LCD
0xd401e2c0      A19     TWSI4        LCD
==============  ======  ===========  ===========  ===========  ===========  ===========  ===========  ===========  ===========
