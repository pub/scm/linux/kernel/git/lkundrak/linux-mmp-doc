.. SPDX-License-Identifier: GPL-2.0-or-later

MMP SoC Hardware Documentation
==============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pins

.. index
