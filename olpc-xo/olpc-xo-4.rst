.. SPDX-License-Identifier: GPL-2.0-or-later

OLPC XO-4
=========

OLPC XO-4 is a MMP3-based laptop. It's reasonably well supported by the
mainline kernel.

Most of what applies to :ref:`olpc-xo:OLPC XO-1.75` applies to XO-4 as well.
This page deals with the differencies.

Pin Assignment
--------------

Certain signals are assigned diferently on XO-4, mostly due to extra
peripherals on XO-4 laptop (such as touch screen and HDMI port) as well as
MMP3 SoC (extra pins for MMC5 controller used for internal SD card slot).

Here's the summary:

==============  ==============  ==============  ==================================================================
Signal          Function                        Note
--------------  ------------------------------  ------------------------------------------------------------------
..              XO-1.75         XO-4
==============  ==============  ==============  ==================================================================
BOOT_DEV_SEL    GPIO_56         GPIO_2          eMMC or Internal SD selector
CAM_SCL         GPIO_108        GPIO_165        OV7670 I2C
CAM_SDA         GPIO_109        GPIO_166        OV7670 I2C
DCON_IRQ#       GPIO_124        GPIO_126        HX8837
DCON_SCL        GPIO_161        GPIO_168        HX8837 I2C
DCON_SDA        GPIO_110        GPIO_167        HX8837 I2C
EB_MODE#        GPIO_128        GPIO_130        Display rotated in e-book mode
EC_SPI_ACK      GPIO_125        GPIO_113        To Embedded Controller
HP_PLUG         GPIO_97         GPIO_13         Headphone
HUB_RESET#      GPIO_146        GPIO_148        USB Hub Reset
VID2            GPIO_11         GPIO_123        Vcore voltage control
WLAN_PD#        GPIO_57         GPIO_35         Wi-Fi Power Down
WLAN_RESET#     GPIO_58         GPIO_36         Wi-Fi Reset
eMMC_RST#       GPIO_149        GPIO_144        eMMC Reset
I2S_SYSCLK      I2S_SYSCLK      APPMU_SYSCLK    Different I2S sysclk. Why?
SD1_CLK         MMC3_CLK        MMC5_CLK        eMMC and Internal SD share MMC3 on XO-1.75. MMC5 only on MMP3
SD1_CMD         MMC3_CMD        MMC5_CMD        〃
SD1_DATA0       MMC3_DATA0      MMC5_DATA_0     〃
SD1_DATA1       MMC3_DATA1      MMC5_DATA_1     〃
SD1_DATA2       MMC3_DATA2      MMC5_DATA_2     〃
SD1_DATA3       MMC3_DATA3      MMC5_DATA_3     〃
EN_eMMC_PWR#                    GPIO_97         eMMC power off on XO-4 can work around MMP3 leakage issue
SOC_SEL                         GPIO_3          MMP3 CPU Speed: 1 - 1GHz, 0 - 1.2GHz
MEM_SZ0                         GPIO_0          Memory size: 0 - 1GB, 1 - 2GB
MEM_SZ1                         GPIO_1          Memory size: always 0
HDMI_HP_DET                     GPIO_14         HDMI port only on XO-4
HDMI_SCL                        GPIO_4          〃
HDMI_DAT                        GPIO_5          〃
TOUCH_BSL_RXD                   UART4_TXD       Touch only on XO-4
TOUCH_BSL_TXD                   UART4_RXD       〃
TOUCH_HD                        GPIO_12         〃
TOUCH_RST#                      GPIO_98         〃
TOUCH_TCK                       GPIO_139        〃
CONSOLE_RXD     UART3_RXD       UART2_RXD       OFW/Linux Console (XO-1.75: CN8, XO-4: CN18)
CONSOLE_TXD     UART3_RXD       UART2_TXD       OFW/Linux Console (XO-1.75: CN8, XO-4: CN18)
==============  ==============  ==============  ==================================================================

