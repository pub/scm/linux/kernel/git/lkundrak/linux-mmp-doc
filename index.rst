.. SPDX-License-Identifier: GPL-2.0-or-later

Linux on Marvell MMP
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   olpc-xo
   olpc-xo/olpc-xo-4
   dell-wyse-3020
   hwdoc/index

.. index

Hardware Support
----------------

===================     =================       ==============  ===================================
Hardware                Model                   Status          Notes
===================     =================       ==============  ===================================
SMP (mmp3)              mmp3-smp                Mainline
L2 Cache (mmp2)         tauros2                 Mainline
L2 Cache (mmp3)         tauros3                 Mainline
USB 2.0                 pxau2o-ehci             Mainline
USB 2.0 PHY (mmp2)      mmp2-usb-phy            Mainline
USB 2.0 PHY (mmp3)      mmp3-usb-phy            Mainline
MMC                     pxav3-mmc               Mainline
Camera Controller       mmp2-ccic               Mainline
Audio DMA               adma-1.0                Mainline
Peripheral DMA          pdma-1.0                Mainline
UART                    xscale-uart             Mainline
GPIO                    mmp2-gpio               Mainline
I2S                     mmp-twsi                Mainline
RTC                     mmp-rtc                 Mainline
SPI                     mmp2-ssp                Mainline
Audio SRAM              mmio-sram               Mainline
3D GPU (mmp2)           gc860                   Mainline        Needs power domains (In progress)
3D GPU (mmp3)           gc2000                  Mainline        Needs power domains (In progress)
3D GPU (mmp3)           gc320                   Mainline        Needs power domains (In progress)
MMP3 Thermal            mmp3-thermal            In progress
Display Controller      armada-lcdc             In progress
USB HSIC PHY (mmp3)     mmp3-hsic-phy           In progress
Audio Clocks            mmp2-audio-clock        In progress
Audio I2S               mmp-sspa                In progress
Randomness Source       mmp2-rng                In progress
===================     =================       ==============  ===================================

.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
