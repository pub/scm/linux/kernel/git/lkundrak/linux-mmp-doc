.. SPDX-License-Identifier: GPL-2.0-or-later
.. highlight:: shell-session

OLPC XO-1.75
============

OLPC XO-1.75 is a MMP2-based laptop. It's reasonably well supported by the
mainline kernel.

This page mostly applies to OLPC XO-4, a similar laptop based on MMP3, as
well. Consult the :ref:`olpc-xo/olpc-xo-4:OLPC XO-4` page for details about
differencies.

Hardware Support
----------------

===================     =================       ==============  ===================================
Hardware                Model                   Status          Notes
===================     =================       ==============  ===================================
Game Keys               gpio-keys               Mainline
Camera                  ov7670                  Mainline
Wi-Fi                   sd8686                  Mainline
Keyboard                AP SP                   Mainline
Embedded Controller     xo1.75-ec               Mainline
Audio Codec             rt5631                  Mainline        Needs the SoC I2C to work
Video Encoder           hx8837                  In progress
LCD Panel               ls075at011              In progress
===================     =================       ==============  ===================================

Firmware security
-----------------

Most OLPC machines were shipped with the security system that disallows
booting unsigned software. If this is the case with your machine, in order
to run the OS images other than OLPC OS you'll need to get a developer key
and deactivate the security system.

The procedure is descriped in the OLPC wiki:
http://wiki.laptop.org/go/Activation_and_Developer_Keys

Firmware upgrade
----------------

It is always preferrable to use an up to date firmware. The firmware images
are available at http://wiki.laptop.org/go/Firmware. For the XO-1.75 laptop
to boot the mainline kernel a firmware Q4E00JA or newer is needed. You can
get it at http://dev.laptop.org/~quozl/q4e00ja.rom.

To update the firmware, place the .rom file on to your bootable media,
connect a charged battery pack and a wall adapter, and enter the Open
Firmware prompt by pressing ESC during the early boot (needs an unlocked
laptop -- see "Firmware security" above). Then use the "flash" command
to update the firmware::

  ok flash ext:\q4e00ja.rom   \ Flash the "q4e00ja.rom" from the SD card
  ok flash u:\q4e00ja.rom     \ Flash the "q4e00ja.rom" from USB stick

Installing Fedora
-----------------

Installing Fedora is very straightforward, since ``arm-image-installer``
supports it out-of-box. You can install it to a USB flash stick or a SD
card.

The process destroys all data on the target volume, so make sure you pick
the right one.  Substitute ``sdx`` in command below for your actual volume.
``lsblk`` command might help you identify which one is it. Once you know
the device name, you're ready to install Fedora::

  # arm-image-installer --resizefs --target=olpc_xo175 --media=/dev/sdx \
      --image=Fedora-Minimal-armhfp-32-1.6-sda.raw.xz
  
  =====================================================
  = Selected Image:
  = Fedora-Minimal-armhfp-32-1.6-sda.raw.xz
  = Selected Media : /dev/sdx
  = U-Boot Target : olpc_xo175
  =====================================================
  
  *****************************************************
  *****************************************************
  ******** WARNING! ALL DATA WILL BE DESTROYED ********
  *****************************************************
  *****************************************************
  
   Type 'YES' to proceed, anything else to exit now
  
  = Proceed? YES
  = Writing:
  = Fedora-Minimal-armhfp-32-1.6-sda.raw.xz
  = To: /dev/sdx ....
  2066571264 bytes (2.1 GB, 1.9 GiB) copied, 91 s, 22.7 MB/s
  0+233502 records in
  0+233502 records out
  2088763392 bytes (2.1 GB, 1.9 GiB) copied, 91.554 s, 22.8 MB/s
  = Writing image complete!
  = Copying loader scripts to boot partition
  
  = Installation Complete! Insert into the olpc_xo175 and boot.
  #

Once your machine is unlocked, it will automatically boot from your media
wherever it will detect it attached to the USB bus or the SD card slot.

Installing Debian
-----------------

Starting with December 2020 debian-installer nightlies, Debian produces
install images for the OLPC XO-1.75 laptops. Debian installer can be
started from a USB device or a SD card that can be prepared in the usual way.
The target you install Debian to can also be a USB device, a SD card, or the
internal eMMC.

If you haven't installed Debian before you may find the full installation
manual at https://d-i.debian.org/manual/en.armhf/index.html helpful.

For a network installation you'll need a USB ethernet adapter and some
files from
`netboot/SD-card-images/ <http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/netboot/SD-card-images/>`_.
Apparently, you can also do an offline image by placing an appropriate
Debian ISO image on the install media and the installation using files from
`hd-media/SD-card-images/ <http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/hd-media/SD-card-images/>`_.
I'm going to assume you're doing a network install because ISO images of
Arm nightlies don't seem to be produced and an online installation is
probably more convenient anyway.

Basically you just need to get
`firmware.OLPC_XO.img.gz <http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/hd-media/SD-card-images/firmware.OLPC_XO.img.gz>`_
and `partition.img.gz <http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/netboot/SD-card-images/partition.img.gz>`_,
uncompress them and concatenate them to your install media.  You can read
more on that in
`README.concatenateable_images <http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/netboot/SD-card-images/README.concatenateable_images>`_.

Here's how you'd go about doing it, assuming your install media is ``sdx``.
The process destroys all data on the target volume, so make sure you pick
the right one. ``lsblk`` command might help you identify the right volume::

  # wget http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/hd-media/SD-card-images/firmware.OLPC_XO.img.gz
  # gunzip firmware.OLPC_XO.img.gz
  # wget http://ftp.debian.org/debian/dists/sid/main/installer-armhf/current/images/netboot/SD-card-images/partition.img.gz
  # gunzip partition.img.gz
  # cat firmware.OLPC_XO.img partition.img >/dev/sdx
  #

That's it, really. Once your machine is unlocked, it will automatically boot
from your install media wherever it will detect it attached to the USB bus
or the SD card slot.

Once Debian is installed, it will be booted automatically as well.
